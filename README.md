# CS371p: Object-Oriented Programming Collatz Repo

- Name: Mark Cheng

- EID: mzc246

- GitLab ID: markzcheng

- HackerRank ID: markzcheng

- Git SHA: acde60036f73d4e2713e9f7db896c86372051c88

- GitLab Pipelines: https://gitlab.com/markzcheng/cs371p-collatz/-/pipelines

- Estimated completion time: 10 hours

- Actual completion time: 13

- Comments: N/A
