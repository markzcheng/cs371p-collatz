// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream> // istringtstream, ostringstream
#include <tuple>   // make_tuple, tuple
#include <utility> // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read)
{
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0)
{
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1)
{
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2)
{
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3)
{
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4)
{
    ASSERT_EQ(collatz_eval(make_pair(95, 100)), make_tuple(95, 100, 119));
}

TEST(CollatzFixture, eval5)
{
    ASSERT_EQ(collatz_eval(make_pair(100, 95)), make_tuple(100, 95, 119));
}

TEST(CollatzFixture, eval6)
{
    ASSERT_EQ(collatz_eval(make_pair(50, 50)), make_tuple(50, 50, 25));
}

TEST(CollatzFixture, eval7)
{
    ASSERT_EQ(collatz_eval(make_pair(999999, 999999)), make_tuple(999999, 999999, 259));
}

TEST(CollatzFixture, eval8)
{
    ASSERT_EQ(collatz_eval(make_pair(8340, 6306)), make_tuple(8340, 6306, 257));
}

TEST(CollatzFixture, eval9)
{
    ASSERT_EQ(collatz_eval(make_pair(9166, 4613)), make_tuple(9166, 4613, 262));
}

TEST(CollatzFixture, eval10)
{
    ASSERT_EQ(collatz_eval(make_pair(173, 98)), make_tuple(173, 98, 125));
}

TEST(CollatzFixture, eval11)
{
    ASSERT_EQ(collatz_eval(make_pair(57, 61)), make_tuple(57, 61, 33));
}

TEST(CollatzFixture, eval12)
{
    ASSERT_EQ(collatz_eval(make_pair(531603, 796110)), make_tuple(531603, 796110, 509));
}

TEST(CollatzFixture, eval13)
{
    ASSERT_EQ(collatz_eval(make_pair(880968, 60796)), make_tuple(880968, 60796, 525));
}

TEST(CollatzFixture, eval14)
{
    ASSERT_EQ(collatz_eval(make_pair(68521, 630768)), make_tuple(68521, 630768, 509));
}

TEST(CollatzFixture, eval15)
{
    ASSERT_EQ(collatz_eval(make_pair(93882, 390106)), make_tuple(93882, 390106, 443));
}

TEST(CollatzFixture, eval16)
{
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

// -----
// print
// -----

TEST(CollatzFixture, print)
{
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve)
{
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
