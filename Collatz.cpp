// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

long cache[1000000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read(const string &s)
{
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------
int highestCycleLength(int start, int end)
{
    int highest = -1;
    //  go through the numbers in range of start to end inclusive
    for (int index = start; index <= end; index++)
    {
        //  keep track of numCycles and stop when number == 1
        int numCycles = 1;
        long number = index;
        while (number != 1)
        {
            /*  if we are not out of bounds and the cache holds a
                non-zero value then we can access the cache */
            if ((number - 1) < 1000000 && cache[number - 1] != 0)
            {
                /*  numCycles has already been found for current number
                    and subtract two to account for numCycles being incremented
                    at the end, and then end loop by setting number to 1 */
                numCycles += cache[number - 1] - 2;
                number = 1;
            }
            //  if number is even then divide number by 2
            else if (number % 2 == 0)
            {
                number = number / 2;
            }
            // else we use the optimization in class and bitshift right by 1
            else
            {
                number = number + (number >> 1) + 1;
                numCycles++;
            }
            numCycles++;
        }
        // store value into cache
        cache[index - 1] = numCycles;
        // update highest if we have a number larger
        if (numCycles > highest)
        {
            highest = numCycles;
        }
    }
    assert(highest > 0);
    return highest;
}

tuple<int, int, int> collatz_eval(const pair<int, int> &p)
{
    int i, j;
    tie(i, j) = p;
    // <your code>
    assert(i > 0);
    assert(j > 0);
    assert(i < 1000000);
    assert(j < 1000000);
    /*  assign larger and smaller to i and j based on
        which ever one is larger */
    int larger = (i > j) ? i : j;
    int smaller = (larger == i) ? j : i;
    //  optimization we learned in class
    int midway = larger / 2 + 1;
    smaller = (smaller < midway) ? midway : smaller;
    //  call helper function to find the largest cycle length
    int highest = highestCycleLength(smaller, larger);
    return make_tuple(i, j, highest);
}

// -------------
// collatz_print
// -------------

void collatz_print(ostream &sout, const tuple<int, int, int> &t)
{
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve(istream &sin, ostream &sout)
{
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
